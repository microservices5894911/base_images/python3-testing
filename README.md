# buzzops/python3-testing

Image used for running tests for Python 3 applications.

See the [changelog](CHANGELOG.md) for version details of pre-installed libraries and python packages.

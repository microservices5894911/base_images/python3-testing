FROM mvijayaragavan/python3:3.9.16-0

LABEL url="https://gitlab.com/microservices5894911/base_images/python3-testing"

COPY --from=docker /usr/local/bin/docker /usr/local/bin/docker
COPY --from=docker /usr/local/bin/docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh

RUN apk --no-cache add curl curl-dev docker-compose gcc jq libffi-dev linux-headers make musl-dev openssl-dev util-linux libpq postgresql-dev \
 && pip3 install bandoleers coverage flake8 \
 && chmod a+x /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT [ "/usr/local/bin/docker-entrypoint.sh" ]
CMD [ "sh" ]

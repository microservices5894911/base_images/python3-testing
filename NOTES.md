# Maintainer notes

The first thing you want to do is build the container locally:

    docker build -t mvijayaragavan/python3-testing:local

Once the image is built, you can update CHANGELOG.md using the package
listings generated during the build process.  The `update-changelog`
script is an attempt to automate the process.  After you have built the
image locally, run the following command:

    ./update-changelog local

It will update CHANGELOG.md in-place leaving you a few TODOs at the top
of the file.
